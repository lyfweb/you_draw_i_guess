import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'Index',
  //   redirect: '/Home'
  // },
  {
    path: '/',
    name: 'Home',
    redirect: '/Hall',
    component: () => import('../views/Home'),
    children: [
      {
        path: '/Hall',
        name: 'House',
        component: () => import('../views/Hall')
      },
    ]
  },
  {
    path: '/House/:id',
    name: 'Detail',
    component: () => import('../views/House/House')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
